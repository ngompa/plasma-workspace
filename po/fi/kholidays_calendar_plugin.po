# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
# Lasse Liehu <lasse.liehu@gmail.com>, 2016.
# Tommi Nieminen <translator@legisign.org>, 2019, 2021.
#
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-05-03 02:20+0000\n"
"PO-Revision-Date: 2021-06-04 23:03+0300\n"
"Last-Translator: Tommi Nieminen <translator@legisign.org>\n"
"Language-Team: Finnish <kde-i18n-doc@kde.org>\n"
"Language: fi\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 20.04.2\n"

#: HolidaysConfig.qml:89
#, kde-format
msgid "Region"
msgstr "Alue"

#: HolidaysConfig.qml:93
#, kde-format
msgid "Name"
msgstr "Nimi"

#: HolidaysConfig.qml:97
#, kde-format
msgid "Description"
msgstr "Kuvaus"

#~ msgid "Search…"
#~ msgstr "Etsi…"

#~ msgid "Search Holiday Regions"
#~ msgstr "Etsi vapaapäiväalueita"
