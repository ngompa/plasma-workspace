# translation of systemmonitor.po to Slovak
# Roman Paholik <wizzardsk@gmail.com>, 2014.
# Ferdinand Galko <galko.ferdinand@gmail.com>, 2023.
msgid ""
msgstr ""
"Project-Id-Version: systemmonitor\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-11-04 01:39+0000\n"
"PO-Revision-Date: 2023-04-11 16:24+0200\n"
"Last-Translator: Ferdinand Galko <galko.ferdinand@gmail.com>\n"
"Language-Team: Slovak <opensuse-translation@opensuse.org>\n"
"Language: sk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 21.12.3\n"
"Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Roman Paholík"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "wizzardsk@gmail.com"

#: kdedksysguard.cpp:40
#, kde-format
msgid "Show System Activity"
msgstr "Zobraziť aktivitu systému"

#: main.cpp:22
#, kde-format
msgid "System Activity"
msgstr "Aktivita systému"
