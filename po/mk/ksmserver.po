# translation of ksmserver.po to Macedonian
#
# Copyright (C) 2000,2002,2003, 2004, 2005, 2007, 2009 Free Software Foundation, Inc.
#
# Danko Ilik <danko@mindless.com>, 2000,2002,2003.
# Novica Nakov <novica@bagra.net.mk>, 2003.
# Bozidar Proevski <bobibobi@freemail.com.mk>, 2004, 2005, 2009.
# Zaklina Gjalevska <gjalevska@yahoo.com>, 2005.
msgid ""
msgstr ""
"Project-Id-Version: ksmserver\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-11-04 01:39+0000\n"
"PO-Revision-Date: 2009-06-30 10:39+0200\n"
"Last-Translator: Bozidar Proevski <bobibobi@freemail.com.mk>\n"
"Language-Team: Macedonian <mkde-l10n@lists.sourceforge.net>\n"
"Language: mk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 1.11.4\n"
"Plural-Forms: Plural-Forms: nplurals=3; plural=n%10==1 ? 0 : n%10==2 ? 1 : "
"2;\n"

#: logout.cpp:340
#, kde-format
msgid "Logout canceled by '%1'"
msgstr "Одјавувањето е откажано од „%1“"

#: main.cpp:74
#, kde-format
msgid "$HOME not set!"
msgstr ""

#: main.cpp:78 main.cpp:86
#, kde-format
msgid "$HOME directory (%1) does not exist."
msgstr ""

#: main.cpp:81
#, kde-kuit-format
msgctxt "@info"
msgid ""
"No write access to $HOME directory (%1). If this is intentional, set "
"<envar>KDE_HOME_READONLY=1</envar> in your environment."
msgstr ""

#: main.cpp:88
#, kde-format
msgid "No read access to $HOME directory (%1)."
msgstr ""

#: main.cpp:92
#, kde-format
msgid "$HOME directory (%1) is out of disk space."
msgstr ""

#: main.cpp:95
#, kde-format
msgid "Writing to the $HOME directory (%2) failed with the error '%1'"
msgstr ""

#: main.cpp:108 main.cpp:143
#, kde-format
msgid "No write access to '%1'."
msgstr ""

#: main.cpp:110 main.cpp:145
#, kde-format
msgid "No read access to '%1'."
msgstr ""

#: main.cpp:118 main.cpp:131
#, kde-format
msgid "Temp directory (%1) is out of disk space."
msgstr ""

#: main.cpp:121 main.cpp:134
#, kde-format
msgid ""
"Writing to the temp directory (%2) failed with\n"
"    the error '%1'"
msgstr ""

#: main.cpp:149
#, kde-format
msgid ""
"The following installation problem was detected\n"
"while trying to start Plasma:"
msgstr ""

#: main.cpp:152
#, kde-format
msgid ""
"\n"
"\n"
"Plasma is unable to start.\n"
msgstr ""

#: main.cpp:159
#, kde-format
msgid "Plasma Workspace installation problem!"
msgstr ""

#: main.cpp:193
#, fuzzy, kde-format
#| msgid ""
#| "The reliable KDE session manager that talks the standard X11R6 \n"
#| "session management protocol (XSMP)."
msgid ""
"The reliable Plasma session manager that talks the standard X11R6 \n"
"session management protocol (XSMP)."
msgstr ""
"Доверливиот менаџер на сесии на KDE што го зборува стандардниот X11R6\n"
"протокол за менаџмент на сесии (XSMP)."

#: main.cpp:197
#, kde-format
msgid "Restores the saved user session if available"
msgstr "Ја обновува зачуваната корисничка сесија доколку постои"

#: main.cpp:200
#, kde-format
msgid "Also allow remote connections"
msgstr "Исто така дозволува оддалечени поврзувања"

#: main.cpp:203
#, kde-format
msgid "Starts the session in locked mode"
msgstr ""

#: main.cpp:207
#, kde-format
msgid ""
"Starts without lock screen support. Only needed if other component provides "
"the lock screen."
msgstr ""

#: server.cpp:802
#, kde-format
msgctxt "@label an unknown executable is using resources"
msgid "[unknown]"
msgstr ""

#: server.cpp:825
#, kde-kuit-format
msgctxt "@label notification; %1 is a list of executables"
msgid ""
"Unable to manage some apps because the system's session management resources "
"are exhausted. Here are the top three consumers of session resources:\n"
"%1"
msgstr ""

#: server.cpp:968
#, fuzzy, kde-format
#| msgid "The KDE Session Manager"
msgid "Session Management"
msgstr "Менаџерот на сесии на KDE"

#: server.cpp:973
#, fuzzy, kde-format
#| msgid "&Logout"
msgid "Log Out"
msgstr "Од&јави се"

#: server.cpp:978
#, kde-format
msgid "Shut Down"
msgstr ""

#: server.cpp:983
#, kde-format
msgid "Reboot"
msgstr ""

#: server.cpp:989
#, kde-format
msgid "Log Out Without Confirmation"
msgstr ""

#: server.cpp:994
#, kde-format
msgid "Shut Down Without Confirmation"
msgstr ""

#: server.cpp:999
#, kde-format
msgid "Reboot Without Confirmation"
msgstr ""

#: server.cpp:1129
#, kde-kuit-format
msgctxt "@label notification; %1 is an executable name"
msgid ""
"Unable to restore <application>%1</application> because it is broken and has "
"exhausted the system's session restoration resources. Please report this to "
"the app's developers."
msgstr ""

#, fuzzy
#~| msgid ""
#~| "Starts 'wm' in case no other window manager is \n"
#~| "participating in the session. Default is 'kwin'"
#~ msgid ""
#~ "Starts <wm> in case no other window manager is \n"
#~ "participating in the session. Default is 'kwin'"
#~ msgstr ""
#~ "Го стартува „wm“ во случај во сесијата да не учествува\n"
#~ "друг менаџер на прозорци. Стандарден е „kwin“."

#, fuzzy
#~| msgid "&Logout"
#~ msgid "Logout"
#~ msgstr "Од&јави се"

#, fuzzy
#~| msgid "Logging out in 1 second."
#~| msgid_plural "Logging out in %1 seconds."
#~ msgid "Sleeping in 1 second"
#~ msgid_plural "Sleeping in %1 seconds"
#~ msgstr[0] "Одјавување за %1 секунда."
#~ msgstr[1] "Одјавување за %1 секунди."
#~ msgstr[2] "Одјавување за %1 секунди."

#~ msgid "Logging out in 1 second."
#~ msgid_plural "Logging out in %1 seconds."
#~ msgstr[0] "Одјавување за %1 секунда."
#~ msgstr[1] "Одјавување за %1 секунди."
#~ msgstr[2] "Одјавување за %1 секунди."

#~ msgid "Turning off computer in 1 second."
#~ msgid_plural "Turning off computer in %1 seconds."
#~ msgstr[0] "Исклучување на компјутерот за %1 секунда."
#~ msgstr[1] "Исклучување на компјутерот за %1 секунди."
#~ msgstr[2] "Исклучување на компјутерот за %1 секунди."

#~ msgid "Restarting computer in 1 second."
#~ msgid_plural "Restarting computer in %1 seconds."
#~ msgstr[0] "Рестартирање на компјутерот за %1 секунда."
#~ msgstr[1] "Рестартирање на компјутерот за %1 секунди."
#~ msgstr[2] "Рестартирање на компјутерот за %1 секунди."

#, fuzzy
#~| msgid "&Turn Off Computer"
#~ msgid "Turn Off Computer"
#~ msgstr "&Исклучи го компјутерот"

#, fuzzy
#~| msgid "&Restart Computer"
#~ msgid "Restart Computer"
#~ msgstr "&Рестартирај го компјутерот"

#~ msgctxt "default option in boot loader"
#~ msgid " (default)"
#~ msgstr " (стандардно)"

#, fuzzy
#~| msgid "&Cancel"
#~ msgid "Cancel"
#~ msgstr "&Откажи"

#~ msgid "&Standby"
#~ msgstr "Сп&ремен"

#~ msgid "Suspend to &RAM"
#~ msgstr "Суспендирај во &РАМ"

#~ msgid "Suspend to &Disk"
#~ msgstr "Суспендирај на &диск"

#~ msgctxt "NAME OF TRANSLATORS"
#~ msgid "Your names"
#~ msgstr "Божидар Проевски"

#~ msgctxt "EMAIL OF TRANSLATORS"
#~ msgid "Your emails"
#~ msgstr "bobibobi@freemail.com.mk"

#~ msgid "(C) 2000, The KDE Developers"
#~ msgstr "(C) 2000, развивачите на KDE"

#~ msgid "Matthias Ettrich"
#~ msgstr "Matthias Ettrich"

#~ msgid "Luboš Luňák"
#~ msgstr "Luboš Luňák"

#~ msgid "Maintainer"
#~ msgstr "Одржувач"
