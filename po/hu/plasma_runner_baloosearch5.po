# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Kristóf Kiszel <ulysses@kubuntu.org>, 2014, 2019.
# Kiszel Kristóf <kiszel.kristof@gmail.com>, 2017.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-07-10 02:12+0000\n"
"PO-Revision-Date: 2019-12-01 18:47+0100\n"
"Last-Translator: Kristóf Kiszel <ulysses@kubuntu.org>\n"
"Language-Team: Hungarian <kde-l10n-hu@kde.org>\n"
"Language: hu\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 20.03.70\n"

#: baloosearchrunner.cpp:60
#, kde-format
msgid "Open Containing Folder"
msgstr "Tartalmazó mappa megnyitása"

#: baloosearchrunner.cpp:82
#, fuzzy, kde-format
#| msgid "Audio"
msgid "Audios"
msgstr "Hang"

#: baloosearchrunner.cpp:83
#, fuzzy, kde-format
#| msgid "Image"
msgid "Images"
msgstr "Kép"

#: baloosearchrunner.cpp:84
#, fuzzy, kde-format
#| msgid "Video"
msgid "Videos"
msgstr "Videó"

#: baloosearchrunner.cpp:85
#, fuzzy, kde-format
#| msgid "Spreadsheet"
msgid "Spreadsheets"
msgstr "Táblázat"

#: baloosearchrunner.cpp:86
#, fuzzy, kde-format
#| msgid "Presentation"
msgid "Presentations"
msgstr "Bemutató"

#: baloosearchrunner.cpp:87
#, fuzzy, kde-format
#| msgid "Folder"
msgid "Folders"
msgstr "Mappa"

#: baloosearchrunner.cpp:88
#, fuzzy, kde-format
#| msgid "Document"
msgid "Documents"
msgstr "Dokumentum"

#: baloosearchrunner.cpp:89
#, fuzzy, kde-format
#| msgid "Archive"
msgid "Archives"
msgstr "Archívum"

#: baloosearchrunner.cpp:90
#, kde-format
msgid "Texts"
msgstr ""

#: baloosearchrunner.cpp:91
#, kde-format
msgid "Files"
msgstr ""

#~ msgid "Search through files, emails and contacts"
#~ msgstr "Keresés fájlokban, e-mailekben és névjegyekben"

#~ msgid "Email"
#~ msgstr "E-mail"
