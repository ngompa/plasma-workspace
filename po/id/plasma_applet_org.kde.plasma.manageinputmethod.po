# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the plasma-workspace package.
# Wantoyèk <wantoyek@gmail.com>, 2022.
#
msgid ""
msgstr ""
"Project-Id-Version: plasma-workspace\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-10-16 01:40+0000\n"
"PO-Revision-Date: 2022-03-13 21:18+0700\n"
"Last-Translator: Wantoyèk <wantoyek@gmail.com>\n"
"Language-Team: Indonesian <kde-i18n-doc@kde.org>\n"
"Language: id\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 21.12.3\n"

#: contents/ui/main.qml:70
#, kde-format
msgctxt "Opens the system settings module"
msgid "Configure Virtual Keyboards..."
msgstr "Konfigurasikan Virtual Keyboard..."

#: contents/ui/main.qml:89
#, kde-format
msgid "Virtual Keyboard: unavailable"
msgstr "Virtual Keyboard: tak tersedia"

#: contents/ui/main.qml:102
#, kde-format
msgid "Virtual Keyboard: disabled"
msgstr "Virtual Keyboard: dinonfungsikan"

#: contents/ui/main.qml:118
#, fuzzy, kde-format
#| msgctxt "Opens the system settings module"
#| msgid "Configure Virtual Keyboards..."
msgid "Show Virtual Keyboard"
msgstr "Konfigurasikan Virtual Keyboard..."

#: contents/ui/main.qml:133
#, kde-format
msgid "Virtual Keyboard: visible"
msgstr "Virtual Keyboard: terlihat"

#: contents/ui/main.qml:147
#, kde-format
msgid "Virtual Keyboard: enabled"
msgstr "Virtual Keyboard: difungsikan"
