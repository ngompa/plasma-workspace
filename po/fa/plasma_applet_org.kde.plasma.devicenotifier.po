# translation of plasma_applet_devicenotifier.po to Persian
# Saied Taghavi <s.taghavi@gmail.com>, 2008.
msgid ""
msgstr ""
"Project-Id-Version: plasma_applet_devicenotifier\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-09-28 01:41+0000\n"
"PO-Revision-Date: 2008-04-18 14:31+0330\n"
"Last-Translator: Saied Taghavi <s.taghavi@gmail.com>\n"
"Language-Team: Persian <kde-i18n-fa@kde.org>\n"
"Language: fa\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"

#: package/contents/ui/DeviceItem.qml:185
#, kde-format
msgctxt "@info:status Free disk space"
msgid "%1 free of %2"
msgstr ""

#: package/contents/ui/DeviceItem.qml:189
#, kde-format
msgctxt ""
"Accessing is a less technical word for Mounting; translation should be short "
"and mean 'Currently mounting this device'"
msgid "Accessing…"
msgstr ""

#: package/contents/ui/DeviceItem.qml:192
#, kde-format
msgctxt ""
"Removing is a less technical word for Unmounting; translation should be "
"short and mean 'Currently unmounting this device'"
msgid "Removing…"
msgstr ""

#: package/contents/ui/DeviceItem.qml:195
#, kde-format
msgid "Don't unplug yet! Files are still being transferred..."
msgstr ""

#: package/contents/ui/DeviceItem.qml:226
#, kde-format
msgid "Open in File Manager"
msgstr ""

#: package/contents/ui/DeviceItem.qml:229
#, kde-format
msgid "Mount and Open"
msgstr ""

#: package/contents/ui/DeviceItem.qml:231
#, kde-format
msgid "Eject"
msgstr ""

#: package/contents/ui/DeviceItem.qml:233
#, kde-format
msgid "Safely remove"
msgstr ""

#: package/contents/ui/DeviceItem.qml:275
#, kde-format
msgid "Mount"
msgstr ""

#: package/contents/ui/FullRepresentation.qml:43
#: package/contents/ui/main.qml:252
#, kde-format
msgid "Remove All"
msgstr ""

#: package/contents/ui/FullRepresentation.qml:44
#, fuzzy, kde-format
#| msgid "1 action for this device"
#| msgid_plural "%1 actions for this device"
msgid "Click to safely remove all devices"
msgstr "%1 کنش برای دستگاه"

#: package/contents/ui/FullRepresentation.qml:185
#, fuzzy, kde-format
#| msgid "Configure New Device Notifier"
msgid "No removable devices attached"
msgstr "پیکربندی آگاه‌ساز دستگاه جدید"

#: package/contents/ui/FullRepresentation.qml:185
#, kde-format
msgid "No disks available"
msgstr ""

#: package/contents/ui/main.qml:56
#, fuzzy, kde-format
#| msgid "Recently plugged devices :"
msgid "Most Recent Device"
msgstr "دستگاه‌های تازه‌متصل‌شده :"

#: package/contents/ui/main.qml:56
#, kde-format
msgid "No Devices Available"
msgstr ""

#: package/contents/ui/main.qml:234
#, fuzzy, kde-format
#| msgid "Configure New Device Notifier"
msgctxt "Open auto mounter kcm"
msgid "Configure Removable Devices…"
msgstr "پیکربندی آگاه‌ساز دستگاه جدید"

#: package/contents/ui/main.qml:260
#, fuzzy, kde-format
#| msgid "Configure New Device Notifier"
msgid "Removable Devices"
msgstr "پیکربندی آگاه‌ساز دستگاه جدید"

#: package/contents/ui/main.qml:275
#, fuzzy, kde-format
#| msgid "Configure New Device Notifier"
msgid "Non Removable Devices"
msgstr "پیکربندی آگاه‌ساز دستگاه جدید"

#: package/contents/ui/main.qml:290
#, kde-format
msgid "All Devices"
msgstr ""

#: package/contents/ui/main.qml:307
#, kde-format
msgid "Show popup when new device is plugged in"
msgstr ""

#, fuzzy
#~| msgid "1 action for this device"
#~| msgid_plural "%1 actions for this device"
#~ msgid "It is currently safe to remove this device."
#~ msgstr "%1 کنش برای دستگاه"

#, fuzzy
#~| msgid "1 action for this device"
#~| msgid_plural "%1 actions for this device"
#~ msgid "1 action for this device"
#~ msgid_plural "%1 actions for this device"
#~ msgstr[0] "%1 کنش برای دستگاه"

#, fuzzy
#~| msgid "1 action for this device"
#~| msgid_plural "%1 actions for this device"
#~ msgid "Click to eject this disc."
#~ msgstr "%1 کنش برای دستگاه"

#, fuzzy
#~| msgid "1 action for this device"
#~| msgid_plural "%1 actions for this device"
#~ msgid "Click to safely remove this device."
#~ msgstr "%1 کنش برای دستگاه"

#, fuzzy
#~| msgid "1 action for this device"
#~| msgid_plural "%1 actions for this device"
#~ msgid "Click to mount this device."
#~ msgstr "%1 کنش برای دستگاه"

#, fuzzy
#~| msgid "Recently plugged devices :"
#~ msgid "Could not mount device %1."
#~ msgstr "دستگاه‌های تازه‌متصل‌شده :"

#~ msgid "<font color=white>Recently plugged devices:</font>"
#~ msgstr "<font color=white>دستگاه‌های تازه‌متصل‌شده:</font>"

#~ msgid "Time to stay on top (s) :"
#~ msgstr "مدت زمانِ رو ماندن (ثانیه) :"

#~ msgid "Number of items displayed (0 for no limit) :"
#~ msgstr "تعداد موارد نمایش‌داده‌شده (۰ برای نامحدود) :"

#~ msgid "Display time of items (0 for no limit) :"
#~ msgstr "مدت زمان نمایش موارد (۰ برای نامحدود) :"
